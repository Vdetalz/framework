<?php

require __DIR__ . '/autoload.php';

$config = \App\Config::instance();

//$user = \App\Models\News::findById(3);
/*$news = new App\Models\News();
$news->text = 'GFkexbkfcm';
$news->name = 'test@GFkexbkfcm';
$news->save();*/
$view = new \App\View();
$view->title = 'News';
$view->news = \App\Models\News::getNews();
//$news = \App\Models\News::getNews();
$view->display(__DIR__ . '/App/templates/index.php');

// Delete entity.
if (isset($_GET['delete']) && ! empty($_GET['delete'])) {
    $dNews = \App\Models\News::findById((int)$_GET['delete']);
    $dNews->delete();
    header("Location: index.php");
}

// Add entity.
if (isset($_GET['add'])) {
    $view = new \App\View();
    $view->action = 'Add';
    $view->display(__DIR__ . '/view/add.php');

    if (isset($_POST['add'])) {
        $dbNews       = new App\Models\News();
        $dbNews->text = $_POST['text'];
        $dbNews->name = $_POST['name'];
        $dbNews->save();
        header("Location: index.php");
    }
}

if (isset($_GET['update'])) {
    $view = new \App\View();
    $view->action = 'Update';
    $view->display(__DIR__ . '/view/add.php');

    if (isset($_POST['add'])) {
        $dbNews       = \App\Models\News::findById((int)$_GET['update']);
        $dbNews->text = $_POST['text'];
        $dbNews->name = $_POST['name'];
        $dbNews->save();
        header("Location: index.php");
    }
}

// Back
if (isset($_POST['back'])) {
    header("Location: index.php");
}
/*$view = new \App\View();
$view->assignNews($news);
$templ = $view->renderTemplate('news.php');
include $templ['template'];*/
