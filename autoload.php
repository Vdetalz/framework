<?php
/**
 * Autoloader.
 * App\Models\User => ./App/Models/User.php
 *
 * @param string $class class name
 *
 * @return avoid
 */
function __autoload($class)
{
    require __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
}
