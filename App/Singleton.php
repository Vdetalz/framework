<?php

namespace App;

trait Singleton
{
    /**
     * Current instance.
     *
     * @var Singleton instance
     */
    protected static $instance;

    /**
     * Singleton constructor.
     */
    private function __construct()
    {

    }

    /**
     * Returns object instance.
     *
     * @return object $instance.
     */
    public static function instance()
    {
        if (null === static::$instance) {
            static::$instance = new static;
        }

        return static::$instance;
    }
}