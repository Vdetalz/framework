<?php

namespace App;

abstract class Model
{
    /**
     * Table name.
     *
     * @const TABLE
     */
    const TABLE = '';

    /**
     * Id.
     *
     * @var int id.
     */
    public $id;

    /**
     * Finds all objects of current entity.
     *
     * @return object.
     */
    public static function findAll()
    {
        /**@var \App\Db */
        $db = Db::instance();

        return $db->query('SELECT * FROM ' . static::TABLE, static::class);
    }

    /**
     * Find by id.
     *
     * @param int $id id.
     *
     * @return object|bool
     *   Returns False if empty query and object if fine query.
     */
    public static function findById($id)
    {
        /**@var \App\Db */
        $db  = Db::instance();
        $sql = 'SELECT * FROM ' . static::TABLE . ' WHERE id=:id';
        $res = $db->query($sql, static::class, ['id' => (int)$id]);

        if (empty($res)) {
            return false;
        }

        return $res[0];
    }

    /**
     * Check existing.
     *
     * @return bool TRUE if exist and FALSE otherwise.
     */
    public function isNew()
    {
        return empty($this->getId());
    }

    /**
     * Inserts data.
     *
     * @return avoid
     */
    public function insert()
    {
        if (!$this->isNew()) {
            return;
        }

        $columns = [];
        $values  = [];

        foreach ($this as $key => $value) {
            if ('id' == $key) {
                continue;
            }
            $columns[]          = $key;
            $values[':' . $key] = $value;
        }

        $sql = 'INSERT INTO ' . static::TABLE . ' (' . implode(',', $columns)
            . ') VALUES (' . implode(',', array_keys($values)) . ')';

        /**@var \App\Db PDO */
        $db = Db::instance();
        $db->execute($sql, $values);

        // Extract id.
        $this->id = $db->getLastId(static::TABLE);
    }

    /**
     * Updates data.
     *
     * @return avoid
     */
    public function update()
    {
        $columns = [];
        $values  = [];

        foreach ($this as $key => $value) {
            if ('id' == $key) {
                continue;
            }
            $columns[]          = $key . ' = :' . $key;
            $values[':' . $key] = $value;
        }
        $values[':id'] = $this->getId();

        $sql = 'UPDATE ' . static::TABLE
            . ' SET ' . implode(',', $columns)
            . ' WHERE id=:id';

        /**@var \App\Db PDO */
        $db = Db::instance();
        $db->execute($sql, $values);

    }

    /**
     * Deletes.
     *
     * @return avoid
     */
    public function delete()
    {
        $sql = 'DELETE FROM ' . static::TABLE
            . ' WHERE id = :id';

        $values[':id'] = $this->getId();

        /**@var \App\Db PDO */
        $db = Db::instance();
        $db->execute($sql, $values);
    }

    /**
     * Saves data.
     *
     * @return avoid
     */
    public function save()
    {
        if (!$this->isNew()) {
            $this->update();

            return;
        }

        $this->insert();
    }


    /**
     * Getter.
     *
     * @return mixed
     */
    abstract public function getName();

    /**
     * Getter.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}