<?php

namespace App\Models;

use \App\Db;
use \App\Model;

class User extends Model
{
    /**
     * Table name.
     *
     * @const TABLE
     */
    const TABLE = 'users';

    /**
     * Name.
     *
     * @var string
     */
    public $name;

    /**
     * Email.
     *
     * @var string
     */
    public $email;

    /**
     * Returns entity's name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}