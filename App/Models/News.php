<?php

namespace App\Models;

use \App\Db;
use \App\Model;

/**
 * Class News
 *
 * @package App\Models
 * @author  Vitaliy <ogegol@gmail.com>
 */
class News extends Model
{
    /**
     * Table name.
     */
    const TABLE = 'news';

    /**
     * Name.
     *
     * @var string
     */
    public $name;

    /**
     * Text.
     *
     * @var string
     */
    public $text;

    /**
     * Getter.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns count news.
     *
     * @param int $count Count news.
     *
     * @return object
     */
    public static function getCountNews($count)
    {

        $db = Db::instance();

        return $db->query(
            'SELECT * FROM ' . static::TABLE . ' ORDER BY id DESC LIMIT '
            . (int)$count, static::class
        );

    }

    /**
     * Returns all news.
     *
     * @return object
     */
    public static function getNews()
    {

        $db = Db::instance();

        return $db->query(
            'SELECT * FROM ' . static::TABLE . ' ORDER BY id DESC',
            static::class
        );

    }

}