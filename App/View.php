<?php

namespace App;

class View
{
    /**
     * Array of view's data and templates.
     *
     * @var $data
     */
    protected $data = [];

    /**
     * Sets undeclared properties.
     *
     * @param string $key   property's name
     * @param mixed  $value property's data
     *
     * @return avoid
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Gets undeclared properties.
     *
     * @param string $key property's name
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->data[$key];
    }

    /**
     * Render template.
     *
     * @param string $template Template's name.
     *
     * @return string
     */
    public function render($template)
    {
        ob_start();
        include $template;
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * Display template.
     *
     * @param string $template Template's name
     *
     *@return avoid
     */
    public function display($template)
    {
        echo $this->render($template);
    }

}