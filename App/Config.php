<?php

namespace App;

use \App\File;

final class Config
{
    use Singleton;

    /**
     * File name.
     *
     * @var string
     */
    protected static $file;

    /**
     * Host name.
     *
     * @var string
     */
    protected static $host = '';

    /**
     * User name.
     *
     * @var string.
     */
    protected static $user = '';

    /**
     * Password.
     *
     * @var string
     */
    protected static $password = '';

    /**
     * DB name.
     *
     * @var string
     */
    protected static $database = '';

    /**
     * @return object App\Config.
     */
    public static function instance()
    {
        if (null === static::$instance) {
            require __DIR__ . '/../frame-config.php';

            self::$host       = self::sanitizeValue($host);
            self::$user       = self::sanitizeValue($user);
            self::$password   = self::sanitizeValue($password);
            self::$database   = self::sanitizeValue($database);
            static::$instance = new static;
        }

        return static::$instance;
    }

    /**
     * Sanitize values.
     *
     * @param string $value Value for the database connection from config.txt.
     *
     * @return string sanitized $value
     */
    protected static function sanitizeValue($value)
    {
        return trim(str_replace("'", "", $value));
    }

    /**
     * Getter.
     *
     * @return string.
     */
    public static function getDatabase()
    {
        return self::$database;
    }

    /**
     * Getter.
     *
     * @return string.
     */
    public static function getHost()
    {
        return self::$host;
    }

    /**
     * Getter.
     *
     * @return string.
     */
    public static function getPassword()
    {
        return self::$password;
    }

    /**
     * Getter.
     *
     * @return string.
     */
    public static function getUser()
    {
        return self::$user;
    }

}