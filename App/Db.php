<?php

namespace App;

class Db
{
    use Singleton;

    /**
     * Dbh.
     *
     * @var \PDO
     */
    protected $dbh;

    /**
     * Db constructor.
     */
    protected function __construct()
    {
        $this->dbh = new \PDO(
            'mysql:host=' . Config::getHost() . '; dbname='
            . Config::getDatabase(), Config::getUser(), Config::getPassword()
        );
    }

    /**
     * Execute.
     *
     * @param string $sql    Query.
     * @param array  $params Substitutions.
     *
     * @return bool
     */
    public function execute($sql, array $params = [])
    {
        $sth = $this->dbh->prepare($sql);
        $res = $sth->execute($params);

        return $res;
    }

    /**
     * Custom query.
     *
     * @param string $sql    Query.
     * @param string $class  Class name.
     * @param array  $params Substitutions.
     *
     * @return array
     */
    public function query($sql, $class, array $params = [])
    {
        $sth = $this->dbh->prepare($sql);

        $res = $sth->execute($params);
        if (false !== $res) {
            return $sth->fetchAll(\PDO::FETCH_CLASS, trim($class));
        }

        return [];
    }

    /**
     * Get last Id.
     *
     * @param string $table table name
     *
     * @return int
     */
    public function getLastId($table)
    {
        return $this->dbh->lastInsertId($table);
    }
}